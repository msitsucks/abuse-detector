import profanity
import sys
import json
"""
Transposition of profane words will work (i guess):
shit - ['siht', 'shti', 's**it', 'sh**t', '$hit', 'sh!t']
"""

if __name__ == '__main__':
    words = json.loads(sys.argv[1])
    finalArr = []
    for i in range(len(words)):
        # tempArr=[]
        for j in range(len(words[i])):
            if words[i][j].isalpha():
                score = profanity.profanityScore(words[i][j])
            else:
                score = 0

            if score == 10:
              finalArr.append(words[i][j])
            # tempArr.append(score)
        # finalArr.append(tempArr)
    print(json.dumps({
      "data": list(set(finalArr))
    }))

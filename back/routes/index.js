var express = require('express');
var router = express.Router();
var spawn = require("child_process").spawn; 
var path=require("path");

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Express' });

  next();
});

router.post("/tokens",(req,res,next)=>{
  var arrTosend=[];

  for(var key in req.body.hash) {
      arrTosend.push(Object.values(req.body.hash[key])[0]);
  }
  
  console.log(arrTosend);
  /*
      Running the python script
  */
  var process = spawn('python',[path.join(__dirname,"../FakeML/example.py"),JSON.stringify(arrTosend)]); 

  // Takes stdout data from script which executed
  // with arguments and send this data to res object 
  process.stdout.on('data', function(data) { 
      console.log(data.toString())
      
      res.send(data.toString()); 
  });

  process.stderr.on('data', (data) => {
      console.log(data.toString());
  });

  // next(); 
});



module.exports = router;

import string
from collections import defaultdict
import json

import utilities

GROUP_SIZE = 2
CHAR_MATCH_HEURISTIC = 0.8

SPECIAL_CHAR_ALIASES = {
    's' : '$',
    'i' : '!',
    'l' : '1',
    'a' : '@',
    'e' : '3',
    'g' : '8',
    'o' : '0',
    'u' : '4'
}

def profanityScore(text):
    profane_word_weights = utilities.readPropertiesFile('profane_words.txt', 'int')
    profane_words = profane_word_weights.keys()

    words_dict = utilities.readPropertiesFile('dict_words.txt', 'list')

    profane_words_transpose = {}
    for w in profane_words:
        profane_words_transpose[w] = getTransposedWords(w)

    words = [w.rstrip('.') for w in text.lower().split()]

    score = 0.0
    words_count = defaultdict(int)

    for w in words:
        inDict = w in words_dict[w[0]]

        w = utilities.rot13(w)

        if w in profane_words:
            words_count[w] += 1

        for pw in profane_words_transpose.keys():
            if w in profane_words_transpose[pw]:
                words_count[pw] += 1

        for pw in profane_words:
            if w.find(pw) != -1 and not inDict:
                words_count[pw] += 1


    for i in range(len(words)-GROUP_SIZE):
        concat_word = words[i] + words[i+1] + words[i+2]
        concat_word = utilities.rot13(concat_word)

        for pw in profane_words:

            if pw.find(concat_word) != -1:
                if checkMatchPercent(len(concat_word), len(pw)):
                    words_count[pw] += 1
                    break

                j = 2
                while True:
                    j += 1
                    if i + j > len(words)-1:
                        break

                    concat_word += utilities.rot13(words[i+j])

                    if pw.find(concat_word) != -1:
                        continue

                    elif checkMatchPercent(len(concat_word)-1, len(pw)):
                        words_count[pw] += 1
                        break

    running_sum = 0
    count = 0
    for w in words_count.keys():
        running_sum += words_count[w] * profane_word_weights[w]
        count += words_count[w]

    if count == 0:
        score = 0
    else:
        score = running_sum/count

    return score


def getTransposedWords(word):

    ret = []

    for i in range(1, len(word)-1):
        ret.append(word[:i] + word[i+1] + word[i] + word[i+2:])

    for i in range(1, len(word)-1):
        ret.append(word[:i] + '**' + word[i+2:])

    for ch in SPECIAL_CHAR_ALIASES.keys():
        if word.find(ch) != -1:
            ret.append(word.replace(ch, SPECIAL_CHAR_ALIASES[ch]))

    return ret

def checkMatchPercent(partialWordLen, wordLen):
    if wordLen <= 4 and partialWordLen >= 3:
            return True
    elif partialWordLen/wordLen > CHAR_MATCH_HEURISTIC:
            return True
    else:
        return False
